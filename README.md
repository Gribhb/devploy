<!-- Headings -->
# Devploy

> Ce script s'inspire en grande parti de la chaîne [youtube Xavki](https://www.youtube.com/channel/UCs_AZuYXi6NA9tkdbhjItHQ)

Ce script permet de déployer des conteneurs très facilement et rapidement. Parfait pour effectuer des tests de nouveaux outils, ou pour mettre en place des environnements de développement. Les conteneurs créés par ce script ont comme image de base, une debian buster avec systemd.

## Requirements
Afin d'utiliser ce script il est obligatoire de construire l'image présent dans le Dockerfile. Pour ce faire, lancer la commande suivante :
```bash
docker build -t deb-systemd:latest .
```

## Utilisation du script
### Afficher l'aide
```bash
./devploy.sh --help
```

### Creation de 1 conteneur
```bash
./devploy.sh --create 1 --name test
```
Cette commande va créer 1 conteneur qui va se nommer *test-dev-1*

### Création de 5 conteneurs
```bash
./devploy.sh --create 5 --name test
```
Cette commande va creer 5 conteneurs qui vont se nommer *test-dev-1* *test-dev-2* *test-dev-3* *test-dev-4* *test-dev-5*

### Suppression des conteneurs test
```bash
./devploy.sh --drop test
```
En reprennant l'exemple de la creation des 5 conteneurs test, cette commande va supprimer les 5 conteneurs *test-dev-1* jusqu'à *test-dev-5*

### Suppression de tous les conteneurs créés avec ce script
```bash
./devploy.sh --clean
```
**ATTENTION SI VOUS AVEZ DEJA DES CONTENEURS QUI CONTIENNENT dev DANS LEUR NOMS CELA LES SUPPRIMERA EGALEMENT**

### Démarrer les conteneurs test
```bash
./devploy.sh --start test
```
### Accéder aux conteneurs 
En ssh :
```bash
ssh root@<ip_du_conteneur>
```

Par la commande docker exec :
```bash
docker exec -it <nom_du_conteneur> /bin/bash
```
### Accéder aux conteneurs depuis l'extérieur (hors host docker)
Typiquement pour accéder à une interface web depuis une machine autre que le host docker.
Pour cela ajouter la règle iptable suivante :
```bash
iptables -t nat -A DOCKER -p tcp --dport <PORT> -j DNAT --to-destination <IP_CONTENEUR>:<PORT> 
```

**Exemple**
```bash
iptables -t nat -A DOCKER -p tcp --dport 80 -j DNAT --to-destination 172.17.0.2:80 
```

**Auteur : GribHB**
Vous pouvez me contacter sur twitter :
@GribHB
