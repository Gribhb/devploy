#!/bin/bash

###############################################################################
#                                                                             #
# Description ; Script permettant de lancer à la volée des conteneurs docker  #
# pour des environnements de tests ou de développement                        #
#                                                                             #
# Auteur : GribHB                                                             #
#                                                                             #
# Date : 18/05/2020                                                           #
#                                                                             #
###############################################################################


#############
# Variables #
#############

# Variable pour définir les options longues
param=`getopt -o d:i:v: --long create:,name:,start:,drop:,infos:,clean:,help: -- "$@"`


##############
# Fonctions #
##############

# Fonction clean
clean(){
	echo -e "\nDébut de la supression"
	docker rm -f $(docker ps -a | grep dev- | awk {'print $1'})
	echo -e "Fin de la supression\n"
}

# Fonction help
aide(){
	echo "
[ARGUMENTS] :
	--create : permet de définir le nombre de conteneurs à créer

	--name   : associer à l'option --create, permet de nommer le conteneur

	--drop   : permet de supprimer le ou les conteneurs en spécifiant le nom (pas besoin de spécifier dev ou le numéro)

	--clean  : permet de supprimer tous les conteneurs créer à partir de ce script (ATTENTION SI D'AUTRES CONTENEURS CONTIENNENT dev DANS LEUR NOMS)

	--start  : permet de démarrer le ou les conteneurs en spécifiant le nom (pas besoin de spécifier dev ou le numéro)

	--infos : permet d'afficher le nom et l'ip du ou des conteneurs en spécifiant le nom (pas besoin de spécifier dev ou le numéro)

	--help   : permet d'afficher l'aide

[USAGE] :
	./devploy.sh --create <number of container> --name <container name>
        exemple : ./devploy.sh --create 3 --name test

	./devploy.sh --drop <container name>
	exemple : ./devploy.sh --drop test

	./devploy.sh --infos <container name>
	exemple : ./devploy.sh --infos test

	./devploy.sh --clean

"
}


########
# Main #
########

# Boucle pour  assigner un paramètre à une option longue
while true; do
	case "$1" in
		--create)
			create="$2";
			shift 2;
		;;

		--name)
			name="$2";
			shift 2;
		;;

		--drop)
			drop="$2";
			shift 2;
		;;

		--start)
			start="$2";
			shift 2;
		;;

		--clean)
			clean
			exit 1
		;;

		--infos)
			infos="$2";
			shift 2;
		;;

		--help)
			aide
			exit 1
		;;


		*)
			shift;
			break
		;;
	esac
done


# Si l'option create est indiquée
if [ "$create" != "" ];then

	# Setting du nombre de conteneurs minimum et maximun
	min=0
	max=1

	# Récupération du nombre maximum de conteneurs portant meme nom que celui spécifié
	idmax=`docker ps -a --format '{{.Names}}' | grep $name | awk -F "-" '{print $3}' | sort -r | head -1`

	# Setup up des variables min et max avec les nouvelles données
	min=$(($idmax +1))
	max=$(($idmax + $create))

	# Permet d'iterer pour creer le nombre de conteneurs voulu + les nommer
	if [ "$name" != "" ];then
		for i in $(seq $min $max);do
            docker run -tid --privileged=true --userns=host --cap-add NET_ADMIN --cap-add SYS_ADMIN --publish-all=true -v /sys/fs/cgroup:/sys/fs/cgroup:ro --name $name-dev-$i --hostname $name-dev-$i deb-systemd:latest

    	    docker exec -it $name-dev-$i /bin/bash -c "service ssh start"

	    ipInfos=`docker inspect -f {{.NetworkSettings.IPAddress}} $name-dev-$i `

	    echo -e "Conteneur $name-dev-$i crée. Son IP est : $ipInfos\n"
	  done
	fi
fi

# Si l'option start est indiquée
if [ "$start" != "" ];then
	docker start $(docker ps -a | grep $start-dev | awk {'print $1'})
	for startedConteneurs in $(docker ps | grep $start-dev | awk '{print $1}');do
		docker inspect -f ' => {{.Name}} : démarré' $startedConteneurs
	done
	echo ""
fi

# Si l'option drop est indiquée
if [ "$drop" != "" ];then
	echo -e "\nSuppression des conteneurs en cours"
	docker rm -f $(docker ps -a | grep $drop-dev | awk '{print $1}')
	echo -e "\n Fin de la suppression\n"
fi


# Si l'option infos est indiquée
if [ "$infos" != "" ];then
	echo -e  "\nInfos concernant les conteneurs : $infos"
	for conteneurs in $(docker ps -a | grep $infos-dev | awk '{print $1}');do
		docker inspect -f '=> {{.Name}} - {{.NetworkSettings.IPAddress}}' $conteneurs
	done
	echo ""
fi

# Si aucun argument n'est indiqué, afficher l'aide
if [[ $# -eq 0 ]] ; then
	echo "Pour afficher l'aide : ./devploy.sh --help"
	exit 1
fi
